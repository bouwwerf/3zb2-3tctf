
Date: November 5th 1998
MOD Title: 3TCTF
Filename: 3tctffinal.exe
Version: 1.00 First Release version
3tctf Email: threeteamctf@hotmail.com
Official web page: http://3tctf.gamepoint.net
Credits:
Programming: DCB-SuperCow!
Skins, textures: [DOL]MissRookie
Maps: 3tctf1, 2, 3, 4, 5, 7, 8, 10 : DOL's MapLAB
Maps: 3tctf6, 9: *NuT5* Lord Darkness

*** INSTALLATION ***
Run the '3tctffinal.exe' and enter your quake2 directory. It will create a '3tctf' directory in your quake2 folder.

*** running 3tctf on your computer ***
If you want to play 3tctf on your own computer just create a new q2 icon and change the 'target' setting in the properties of your icon; 
<Drive letter>:\quake2\quake2.exe +set game 3tctf +menu_joinserver
Remember this game is for multiplayer only, running a map on your computer will enable you to explore the maps though. This setting will also open with the multiplayer 'join server' menu.

You can run a 3tctf demo by typing 'map 3tdemo.dm2' in your console (you must have started with the +set game 3tctf setting).

*** joining a 3tctf server ***
If you've found a 3tctf server, enter this IP or URL address to the multiplayer->join server->address book. Select 'refresh server list' and select the found server in the menu (you must be connected to the internet). You'll now connect to a 3tctf server. However: If you select a normal server like CTF or Lithium, quake2 will reset to that game and you will join that server. You can also type 'connect' with the address of the server in the console.
 
To connect to the official 3tctf server enter: connect 129.125.13.2:27950

*** entering game ***
If you enter a game, a menu will open. You can select to join a team or become a observer. You can also join a team by typing 'team red, team blue or team green' in the console. If you want to become a observer during the game enter 'observer' in the console. You will lose your frags however. Pressing TAB will switch the menu on/off while in observer mode, you can also type 'inven' in the console.

*** bindings you *must* have ****
bind f "drop flag"
bind g "use grapple"
bind r "say_team"
(keys f,g,r may be changed by you)

*** Differences between Zoid's CTF and 3TCTF ***
- ZBot AutoKick. This is a double secure anti ZBot protection. The server can NOT disable this. ZBot is NOT allowed during play of 3tctf. War on ZBot! 
- You can carry 2 flags and make double captures, this will give the carrier 40 frags point for capturing 2 flags
- 2 carriers can make a double capture too, it must be within 20 seconds after the first capture.
- Drop tech and pick them up again. (note: timeout on pickup is 2 seconds)
- Dropping flags: bind f "drop flag" will enable you to drop flags. (all flags will be dropped)
- BFG on/off (Server can disable BFG, you can still pickup the BFG, but it will not fire, it actually makes a funny sound instead)
- 3 teams? 
- You can't pickup flags of teams that don't have players, unless the flag was dropped.
- 2 new techs (see below)
- well everything else is the same, even the grapple. I *really* like Zoid's CTF 8)

*** Tech Vampire ***
This tech is almost the same as the one in Lithium. If you shoot a opponent, you will get his lost health divided by 2.

*** Tech Ammogen ***
Shoot a opponent and it will drop a ammopack. However: It is not a normal ammopack. It will not enable you to carry more ammo as a normal ammopack would do. You'll get extra ammo of any picked up ammo unless you have run out of that particular type of ammo. E.g. if you have no rockets, you won't receive no new rockets. Behavior is the same as the normal ammopack, though.

*** Extra Server settings for 3tctf ***
instantweap: instant weapon,  0=off, 1=on (default 0)
allow_bfg: Enable BFG,  0=off, 1=on (default 1)
allow_dropflag, enable flag dropping, (default 1)
sv_maplist and sv_maplist2, Enter a custom map cycle list. (if empty, it will cycle through maps 1 to 10), (default: "")
note: sv_maplist(2) have a limit length of 64 letters, use sv_maplist2 if you run out of letters. You can't set the sv_maplist properly with RCON of console (will be fixed in new version)
*Make* sure you enter valid map names! Failure to do so may crash the server in a not found map!
URL, Enter you URL of your WebPages, (default: "unknown")
location, Enter location of your server, (default: "unknown") 
Note: you can find these setting in the 'server.cfg' file in your quake2/3tctf folder.

History of versions:
0.30 first version
0.32 added green banner feature
     some bug fixes
     added the names of flagholder on the HUD
0.34 added instantweap server setting (0 = off, 1 = on)
     default = 0
     default for idview is now ON (id_state)
     added feature: 2 players can capture each one flag and still get a double capture. As long it's 
     within 20 seconds after the first!
     some bug fixes
0.40 working on skinpassword (removed and looking at other solution)
0.42 added allow_bfg server setting (default = 1)
0.43 Removed bug, my fault.. 
     it always respawned all flags when a capture was made,
     even if a 3rd flag was still carried by a other carrier. Bug killed!
0.44 resumed work on skin password and finished
0.45 Anti-flood messages added
0.46 added 'drop flag' (not drop dead 8)
     added allow_dropflag server setting (default = 1)
     fixed a bug reported by [q2coding] mailing list about powershield turned off
     when picking up a power shield, while having cells. (bug was in the original DM code)
0.47 added 'sv_maplist' to loop maps.
     default setting for now: '3tctf1 3tctf2 3tctf3 3tctf4'
     Creditsmenu added 8)
0.48 Fixed the Flooting message bug. 
     Change the init. of the playernames of flagcarriers, maybe fixing the linux bug?
0.49 Fixed a bug: location of %l in say_team didn't work correctly on the green flag location
     Flagcarriers name moved 16 pixels to the left on the HUD.
0.50 Added the Anti-Zbot code.
     Changed sv_maplist code.
0.51 Removed 2 bugs:
     Server crashed after a green team captured the red flag and then the blue flag, fixed
     (bug reported by MissRookie 8)
     Double capture of 2 flagcarrier within 20 seconds didn't work right, fixed.
0.52 Added 'observer' command.
     Added 'ctf_3tctfwebsite' cvar (read only), this is viewable in gamespy or other spies
     sv_maplist set to "" instead of the first 4 maps.
     Because of limited length of 64 chars of sv_maplist I've added the sv_maplist2
     (all maps 3tctf1 to 3tctf10 wouldn't even fit into the sv_maplist!)
0.53 Working on the vampire tech 8)
     Fixed a bug that is in every CTF mod, the bug that you can remove flags from maps..
0.54 Traced back a bug that started at v0.48; names and skins weren't set right... damn bugs 8(
0.55 Fixed the vampire tech and changed it a bit.
0.56 Hopefully removed R_CullAliasModel models/objects/gibs/skull/tris.md2: no such frame 119 bug
     Fixed the skins so they are forced to male/ female/ cyborg/ crakhor/ VWEB should be working now
0.57 Sourcecode taken back from 0.54 and started over again.
     No longer forcing the skins, but forcing VWEB to check skin if valid for VWEB instead.
     Skin password is cleared on clientconnect (caused a new client to get access to forbidden skins)
     Clear the ent->client->resp.ctf_skinpassword on connect added. (bugfix)
     Fixed the chasecam code
     Removed any use of code 'ThrowClientHead', fixing the missing model stuff?
     Fixed the on/off of the logo of the last capture
0.58 Added ThrowClientHead back into the code.
     Found the bug that caused the missing model, as I suspected: VWEB code changed the models frame
     Fixed this by checking if the model is solid.
0.59 Added code for 6th tech AmmoGen.
0.60 Fixed VWEB again, now make a default setting for unknown skin to players/male/weapon.md2
     Added skins of the clan 'Da Clown'
0.61 Added 'features' cvar_t serverinfo added, can't not be changed.
     Fixed protection flagcarrier bug.
     Added that you can't pickup flags of empty teams, even when they are dropped.
1.00 First release.
DCB-SuperCow! (Tech models)