#include "bot.h"
#include "m_player.h"


qboolean Get_YenPos(char *Buff,int *curr)
{
	int i;

	i = *curr + 1;

	while(1)
	{
//		if(i >= strlen(Buff)) return false;
		if(Buff[i] == 0 || Buff[i] == 10 || Buff[i] == 13)
		{
			*curr = i;
			return true;
		}
		if(Buff[i] == '\\')
		{
			*curr = i;
			return true;
		}
		if(Buff[i] == '\t') Buff[i] = 0;
		i++;
	}
}
//----------------------------------------------------------------
//Load Bot Info
//
// Load bot's infomation from 3ZBConfig.cfg
//
//----------------------------------------------------------------
void Load_BotInfo()
{
	char	MessageSection[50];
	char	Buff[1024];
	int		i,j,k,l;

	FILE	*fp;

	SpawnWaitingBots = 0;
	ListedBotCount = 0;

	//init message
	memset(ClientMessage,0,sizeof(ClientMessage));
	//set message section
	if(!ctf->value && chedit->value) strcpy(MessageSection,MESS_CHAIN_DM);
	else if(ctf->value && !chedit->value) strcpy(MessageSection,MESS_CTF);
	else if(ctf->value && chedit->value) strcpy(MessageSection,MESS_CHAIN_CTF);
	else strcpy(MessageSection,MESS_DEATHMATCH);

	//init botlist
	ListedBots = 0;
	j = 1;
	for(i = 0;i < MAXBOTS;i++)
	{
		//netname
		sprintf(Buff,"Zigock[%i]",i);
		strcpy(Bot[i].netname,Buff);
		//model
		strcpy(Bot[i].model,"male");
		//skin
		strcpy(Bot[i].skin,"grunt");
	
		//param
		Bot[i].param[BOP_WALK] = 0;
		Bot[i].param[BOP_AIM] = 5;
		Bot[i].param[BOP_PICKUP] = 5;
		Bot[i].param[BOP_COMBATSKILL] = 5;
		Bot[i].param[BOP_ROCJ] = 0;
		Bot[i].param[BOP_VRANGE] = 90;
		Bot[i].param[BOP_HRANGE] = 180;
		Bot[i].param[BOP_REACTION] = 0;

		//spawn flag
		Bot[i].spflg = 0;
		//team
		Bot[i].team = j;
		if(++j > 3) j = 1; // lb - check for 3 teams
	}

	//botlist value
	botlist = gi.cvar ("botlist", "default", CVAR_SERVERINFO | CVAR_LATCH);
	gamepath = gi.cvar ("game", "0", CVAR_NOSET);

	//load info
	sprintf(Buff,".\\%s\\3ZBconfig.cfg",gamepath->string);
	fp = fopen(Buff,"rt");
	if(fp == NULL)
	{
		gi.dprintf("3ZB CFG: file not found.\n");
		return;
	}
	else
	{
		fseek( fp, 0, SEEK_SET);	//先頭へ移動
		while(1)
		{
			if(fgets( Buff, sizeof(Buff), fp ) == NULL) goto MESS_NOTFOUND;
			if(!_strnicmp(MessageSection,Buff,strlen(MessageSection))) break;
		}

		while(1)
		{
			if(fgets( Buff, sizeof(Buff), fp ) == NULL) goto MESS_NOTFOUND;
			if(Buff[0] == '.' || Buff[0] == '[' || Buff[0] == '#') break;
			k = strlen(Buff);
			if((strlen(Buff) + strlen(ClientMessage)) > MAX_STRING_CHARS - 1) break;
			strcat(ClientMessage,Buff);
		}
MESS_NOTFOUND:
		//if(botlist->string == NULL) strcpy(MessageSection,BOTLIST_SECTION_DM);
		//else 
		sprintf(MessageSection,"[%s]",botlist->string);
		fseek( fp, 0, SEEK_SET);	//先頭へ移動
		while(1)
		{
			if(fgets( Buff, sizeof(Buff), fp ) == NULL)
			{
				MessageSection[0] = 0;
				break;
			}
			if(!_strnicmp(MessageSection,Buff,strlen(MessageSection))) break;
		}
		//when not found
		if(MessageSection[0] == 0)
		{
			strcpy(MessageSection,BOTLIST_SECTION_DM);
			fseek( fp, 0, SEEK_SET);	//先頭へ移動
			while(1)
			{
				if(fgets( Buff, sizeof(Buff), fp ) == NULL) goto BOTLIST_NOTFOUND;
				if(!_strnicmp(MessageSection,Buff,strlen(MessageSection))) break;
			}
		}

		i = 0;
		for(i = 0;i < MAXBOTS;i++)
		{
			if(fgets( Buff, sizeof(Buff), fp ) == NULL) break;
			if(Buff[0] == '[') break;
			if(Buff[0] == '\n' || Buff[0] == '#') {i--;continue;}
			j = 2,k = 1;
			if(!strncmp(Buff,"\\\\",2))
			{
				//netname
				if(Get_YenPos(Buff,&k))
				{
					Buff[k] = 0;
					if(strlen(&Buff[j]) < 21) strcpy(Bot[i].netname,&Buff[j]);
					j = k + 1;
				}
				else break;
				//model name
				if(Get_YenPos(Buff,&k))
				{
					Buff[k] = 0;
					if(strlen(&Buff[j]) < 21) strcpy(Bot[i].model,&Buff[j]);
					j = k + 1;
					k++;
				}
				else break;
				//skin name
				if(Get_YenPos(Buff,&k))
				{
					Buff[k] = 0;
					if(strlen(&Buff[j]) < 21) strcpy(Bot[i].skin,&Buff[j]);
					j = k + 1;
					k++;
				}
				else break;
				for(l = 0;l < MAXBOP;l++)
				{
					//param0-7
					if(Get_YenPos(Buff,&k))
					{
						Buff[k] = 0;
						Bot[i].param[l] = (unsigned char)atoi(&Buff[j]);
						j = k + 1;
						k++;
					}
					else break;
				}
				if(l < MAXBOP) break;
				//team
				if(Get_YenPos(Buff,&k))
				{
					Buff[k] = 0;
					if(Buff[j] == 'R') Bot[i].team = 1;
					else if(Buff[j] == 'B') Bot[i].team = 2;
					else if(Buff[j] == 'G') Bot[i].team = 3; // lb - green team
					else Bot[i].team = 1;
					j = k + 1;
					k++;
				}
				else break;
				//auto spawn
				if(Get_YenPos(Buff,&k))
				{
					Buff[k] = 0;
					Bot[i].spflg = atoi(&Buff[j]);
//gi.dprintf("%i %s\n",Bot[i].spflg,&Buff[j]);
					if(Bot[i].spflg == BOT_SPRESERVED && autospawn->value && !chedit->value) SpawnWaitingBots++; 
					else Bot[i].spflg = BOT_SPAWNNOT;
				}
				else break;
				ListedBots++;
			}
		}
	}
BOTLIST_NOTFOUND:
	fclose(fp);

	gi.dprintf("%i of Bots is listed.\n",ListedBots);	
	spawncycle = level.time + FRAMETIME * 100;
}

//----------------------------------------------------------------
//Get Number of Client
//
// Total Client
//
//----------------------------------------------------------------

int Get_NumOfPlayer (void) //Botも含めたplayerの数
{
	int i,j;
	edict_t *ent;

	j = 0;
	for (i=0 ; i<maxclients->value ; i++)
	{
		ent = g_edicts + 1 + i;
		if (ent->inuse)	j++;
	}
	return j;
}

//----------------------------------------------------------------
//Get New Client
//
// Get new client edict
//
//----------------------------------------------------------------

edict_t *Get_NewClient (void)
{
	int			i;
	edict_t		*e;
	gclient_t	*client;

	e = &g_edicts[(int)maxclients->value];
	for ( i = maxclients->value ; i >= 1  ; i--, e--)
	{
		client = &game.clients[i - 1];
		// the first couple seconds of server time can involve a lot of
		// freeing and allocating, so relax the replacement policy
		if (!e->inuse && !client->pers.connected && ( e->freetime < 2 || level.time - e->freetime > 0.5 ) )
		{
			G_InitEdict (e);
			return e;
		}
	}
	gi.error ("ED_Alloc: no free edicts shit");
	return NULL;
}


//----------------------------------------------------------------
//Bot Think
//
// Bot's think code
//
//----------------------------------------------------------------
void Bot_Think (edict_t *self)
{
	gclient_t	*client;

	if (self->linkcount != self->monsterinfo.linkcount)
	{
//		self->monsterinfo.linkcount = self->linkcount;
		M_CheckGround (self);
	}

	if(self->deadflag)
	{
		if(self->client->ctf_grapple) CTFPlayerResetGrapple(self);

		if(self->s.modelindex == skullindex || self->s.modelindex == headindex) self->s.frame = 0;
		else if(self->s.frame < FRAME_crdeath1 && self->s.frame != 0) self->s.frame = FRAME_death308;
		self->s.modelindex2 = 0;	// remove linked weapon model
//ZOID
		self->s.modelindex3 = 0;	// remove linked ctf flag
//ZOID

		self->client->zc.route_trace = false;
		if(self->client->respawn_time <= level.time)
		{
			if(self->svflags & SVF_MONSTER)
			{
				self->client->respawn_time = level.time;
				CopyToBodyQue (self);
				PutBotInServer(self);
			}		
		}
	}
	else
	{
		Bots_Move_NORM (self);
		if(!self->inuse) return;			//removed botself

		// lb - apply AutoDoc on bots
		CTFApplyRegeneration(self);

		// lb - 3tctf techs
		CTFApplyAmmoGen(self);
		CTFApplyPowerShieldTech(self);
		
		// lb - check for base attacks
		if (self->client->zc.basecheck_framenum)
		{
			if (level.framenum > self->client->zc.basecheck_framenum)
			{
				self->client->zc.basecheck_framenum = level.framenum + 9 + (genrand_int32() % 8);
				Bot_ChatBaseCheck(self);
			}
		}

		client = self->client;

		ClientBeginServerFrame (self);
	}
	if (self->linkcount != self->monsterinfo.linkcount)
	{
//		self->monsterinfo.linkcount = self->linkcount;
		M_CheckGround (self);
	}
	M_CatagorizePosition (self);
	BotEndServerFrame (self);
	self->nextthink = level.time + FRAMETIME;
	return;	
}

//----------------------------------------------------------------
//Initialize Bot
//
// Initialize bot edict
//
//----------------------------------------------------------------

void InitializeBot (edict_t *ent,int botindex )
{
	gclient_t	*client;
	char		pinfo[200];
	int			index;
//	int			i;

	index = ent-g_edicts-1;
	ent->client = &game.clients[index];

	client = ent->client;

	memset (&client->zc,0,sizeof(zgcl_t));
	memset (&client->pers, 0, sizeof(client->pers));
	memset (&client->resp, 0, sizeof(client->resp));

	//set botindex NO.
	client->zc.botindex = botindex;	

	client->resp.enterframe = level.framenum;

	//set netname model skil and CTF team
	sprintf(pinfo,"\\rate\\25000\\msg\\1\\fov\\90\\skin\\%s/%s\\name\\%s\\hand\\0",Bot[botindex].model,Bot[botindex].skin,Bot[botindex].netname);
	ent->client->resp.ctf_team = G_LowestTeam();

	ClientUserinfoChanged (ent, pinfo);

	client->pers.health			= 100;
	client->pers.max_health		= 100;

	client->pers.max_bullets	= 200;
	client->pers.max_shells		= 100;
	client->pers.max_rockets	= 50;
	client->pers.max_grenades	= 50;
	client->pers.max_cells		= 200;
	client->pers.max_slugs		= 50;

	ent->client->pers.connected = false;
	ent->client->pers.isbot = true;

	// lb - 3tctf
	G_PlayerSpawnInit(ent);

	gi.dprintf ("%s connected\n", ent->client->pers.netname);
//	gi.bprintf (PRINT_HIGH, "%s entered the game\n", ent->client->pers.netname);

	if(ctf->value)	gi.bprintf(PRINT_HIGH, "%s joined the %s team.\n",
			client->pers.netname, CTFTeamName(ent->client->resp.ctf_team));
	else 	gi.bprintf (PRINT_HIGH, "%s entered the game\n",
			client->pers.netname);

	// lb - be nice and say hi
	Bot_ChatEnterTheGame(ent);
}

static qboolean moveEntUntilNotStartsolid(edict_t *ent, int contentmask, float step_x, float step_y, float step_z, int max_steps)
{
	trace_t trace;

	while (max_steps-- > 0)
	{
		trace = gi.trace(ent->s.origin, ent->mins, ent->maxs, ent->s.origin, ent, contentmask);
		if (!trace.startsolid)
		{
			return true;
		}

		ent->s.origin[0] += step_x;
		ent->s.origin[1] += step_y;
		ent->s.origin[2] += step_z;
	}

	return false;
}

void PutBotInServer (edict_t *ent)
{
//	edict_t		*touch[MAX_EDICTS];
	int			i, j;//, entcount;
	gitem_t		*item;
	gclient_t	*client;
	vec3_t	spawn_origin, spawn_angles;

	zgcl_t		*zc;		
	
	zc = &ent->client->zc;

//test
//	item = FindItem("Trap");
//	ent->client->pers.inventory[ITEM_INDEX(item)] = 100;
//test	

	//current weapon
	client = ent->client;
	item = Fdi_BLASTER;//FindItem("Blaster");
	client->pers.selected_item = ITEM_INDEX(item);
	client->pers.inventory[client->pers.selected_item] = 1;
	client->pers.weapon = item;
	client->pers.isbot = true;
	client->silencer_shots = 0;
	client->weaponstate = WEAPON_READY;
	client->newweapon = NULL;

	//clear powerups
	client->quad_framenum = 0;
	client->invincible_framenum = 0;
	client->breather_framenum = 0;
	client->enviro_framenum = 0;
	client->grenade_blew_up = false;
	client->grenade_time = 0;

	j = zc->botindex;
	i = zc->routeindex;
	memset (&client->zc,0,sizeof(zgcl_t));
	zc->botindex = j;
	zc->routeindex = i;

//ZOID
	client->ctf_grapple = NULL;

	item = FindItem("Grapple");
	if(ctf->value)	client->pers.inventory[ITEM_INDEX(item)] = 1; //ponpoko
//ZOID

	// lb - clear
	zc->chattime = 0;

	// clear entity values
	ent->classname = "player";
	ent->movetype = MOVETYPE_STEP;
	ent->solid = SOLID_BBOX;
	ent->model = "players/male/tris.md2";
	VectorSet (ent->mins, -16, -16, -24);
	VectorSet (ent->maxs, 16, 16, 32);

	ent->health = ent->client->pers.health;
	ent->max_health = ent->client->pers.max_health;
	ent->gib_health = -40;

	ent->mass = 200;
	ent->target_ent = NULL;
	ent->s.frame = 0;

	// clear entity state values
	ent->s.modelindex = 255;		// will use the skin specified model
	ent->s.skinnum = ent - g_edicts - 1;
	ShowGun(ent);					// ### Hentai ### special gun model

	ent->s.sound = 0;

	ent->monsterinfo.scale = MODEL_SCALE;

	ent->pain = player_pain;
	ent->die = player_die;
	ent->touch = NULL; 
	
	ent->moveinfo.decel = level.time;
	ent->pain_debounce_time = level.time;
	ent->targetname = NULL;

	ent->moveinfo.speed = 1.0;	//ジャンプ中の移動率について追加

	ent->prethink = NULL;
	ent->think = Bot_Think;
	ent->nextthink = level.time + FRAMETIME;
	ent->svflags = 0; // temporary, for SelectSpawnPoint
	ent->s.renderfx = 0;
	ent->s.effects = 0;

	SelectSpawnPoint(ent, spawn_origin, spawn_angles); // warning, adds extra height if SVF_MONSTER
	VectorCopy (spawn_origin, ent->s.origin);
	VectorCopy (spawn_angles, ent->s.angles);

	ent->s.origin[2] += 1;  // make sure off ground
	VectorCopy(ent->s.origin, ent->s.old_origin);
	ent->groundentity = NULL;

	ent->svflags = SVF_MONSTER;

	{
		qboolean success = moveEntUntilNotStartsolid(ent, MASK_BOTSOLIDX, 0, 0, 1, 16);  // try straight up first
		if (!success)
		{
			vec3_t forward;

			VectorCopy(ent->s.old_origin, ent->s.origin);
			AngleVectors(ent->s.angles, forward, NULL, NULL);
			success = moveEntUntilNotStartsolid(ent, MASK_BOTSOLIDX, forward[0], forward[1], 1, 16);  // try up plus spawn angle

			if (!success)
			{
				VectorCopy(ent->s.old_origin, ent->s.origin);
			}
		}

		if (success)
		{
			VectorCopy(ent->s.origin, ent->s.old_origin);
		}
	}

	VectorSet(ent->velocity,0,0,0);
	VectorSet(ent->client->oldvelocity, 0, 0, 0);
	ent->moveinfo.speed = 0;
	ent->client->ps.pmove.pm_flags &= ~PMF_DUCKED;

	Set_BotAnim(ent,ANIM_BASIC,FRAME_run1,FRAME_run6);
	client->anim_run = true;

	ent->client->ctf_grapple = NULL;
	ent->client->quad_framenum = level.framenum;
	ent->client->invincible_framenum = level.framenum;
	ent->client->enviro_framenum = level.framenum;
	ent->client->breather_framenum = level.framenum;
	ent->client->weaponstate = WEAPON_READY;
	ent->takedamage = DAMAGE_AIM;
	ent->air_finished = level.time + 12;
	ent->clipmask = MASK_PLAYERSOLID;//MASK_MONSTERSOLID;
	ent->flags &= ~FL_NO_KNOCKBACK;

	ent->client->anim_priority = ANIM_BASIC;
//	ent->client->anim_run = true;
	ent->s.frame = FRAME_run1-1;
	ent->client->anim_end = FRAME_run6;
	ent->deadflag = DEAD_NO;
	ent->svflags &= ~SVF_DEADMONSTER;

	zc->waitin_obj = NULL;
	zc->first_target = NULL;
	zc->first_target = NULL;
	zc->zcstate = STS_IDLE;

	// lb - new stuff
	zc->basecheck_framenum = level.framenum + 11 + (genrand_int32() % 15);

	if(ent->client->resp.enterframe == level.framenum && !chedit->value)
	{
		gi.WriteByte (svc_muzzleflash);
		gi.WriteShort (ent-g_edicts);
		gi.WriteByte (MZ_LOGIN);
		gi.multicast (ent->s.origin, MULTICAST_PVS);
	}
	else if(!chedit->value)
	{
		gi.WriteByte (svc_muzzleflash);
		gi.WriteShort (ent-g_edicts);
		gi.WriteByte (MZ_RESPAWN);
		gi.multicast (ent->s.origin, MULTICAST_PVS);
	}

	if(ctf->value)
	{
		CTFPlayerResetGrapple(ent);
		client->zc.ctfstate = CTFS_OFFENCER;
	}

	if (!KillBox(ent))
	{
		// could't spawn in?
	}

	gi.linkentity (ent);
	G_TouchTriggers (ent);
}

//----------------------------------------------------------------
//Spawn Bot
//
// spawn bots
//
//	int i	index of bot list
//
//----------------------------------------------------------------

qboolean SpawnBot(int i)
{
	edict_t		*bot,*ent;
	int			k,j;


//gi.cprintf (NULL,PRINT_HIGH,"Called %s %s %s\n",Bot[i].netname,Bot[i].model,Bot[i].skin);
//return false;	

	if(	Get_NumOfPlayer () >= game.maxclients )
	{
		gi.cprintf (NULL,PRINT_HIGH,"Can't add bots\n");
		return false;
	}

	bot = Get_NewClient();
	if(bot == NULL) return false;

	InitializeBot( bot , i);
	PutBotInServer ( bot );

	j = targetindex;
	if(chedit->value)
	{
		for(k = CurrentIndex - 1;k > 0 ;k--)
		{
			if(Route[k].index == 0) break;

			if(Route[k].state == GRS_NORMAL)
			{
				if(--j <= 0) break;
			}  
		}

		bot->client->zc.rt_locktime = level.time + FRAMETIME * 20;
		bot->client->zc.route_trace = true;
		bot->client->zc.routeindex = k;
		VectorCopy(Route[k].Pt,bot->s.origin);
		VectorAdd (bot->s.origin, bot->mins, bot->absmin);
		VectorAdd (bot->s.origin, bot->maxs, bot->absmax);
		bot->client->ps.pmove.pm_flags |= PMF_DUCKED;
		gi.linkentity (bot);
//		bot->s.modelindex = 0;

		gi.WriteByte (svc_muzzleflash);
		gi.WriteShort (bot-g_edicts);
		gi.WriteByte (MZ_LOGIN);
		gi.multicast (bot->s.origin, MULTICAST_PVS);

		ent = &g_edicts[1];
		if(ent->inuse && ent->client && !(ent->svflags & SVF_MONSTER))
		{
			ent->takedamage = DAMAGE_NO;
			ent->movetype = MOVETYPE_NOCLIP;
			ent->target_ent = bot;
			ent->solid = SOLID_NOT;
			ent->client->ps.pmove.pm_type = PM_FREEZE;
			ent->client->ps.pmove.pm_flags |= PMF_NO_PREDICTION ;
			VectorCopy(ent->s.origin,ent->moveinfo.start_origin);
		}
	}

	return true;
}

//----------------------------------------------------------------
//Spawn Call
//
// spawn bots
//
//	int i	index of bot list
//
//----------------------------------------------------------------
void Bot_SpawnCall()
{
	int i;

	for(i = 0;i < MAXBOTS;i++)
	{
		if(Bot[i].spflg == BOT_SPRESERVED)
		{
			if(SpawnBot(i)) Bot[i].spflg = BOT_SPAWNED;
			else 
			{
				Bot[i].spflg = BOT_SPAWNNOT;
				targetindex = 0;
			}
			SpawnWaitingBots--;
			break;
		}
	}
}
//----------------------------------------------------------------
//Spawn Bot Reserving
//
// spawn bots reserving
//
//----------------------------------------------------------------
void SpawnBotReserving()
{
	int	i;

	for(i = 0;i < MAXBOTS; i++)
	{
		if(Bot[i].spflg == BOT_SPAWNNOT)
		{
			Bot[i].spflg = BOT_SPRESERVED;
			SpawnWaitingBots++;
			return;
		}
	}
	gi.cprintf (NULL, PRINT_HIGH, "Now max of bots(%i) already spawned.\n",MAXBOTS);
}
//----------------------------------------------------------------
//Spawn Bot Reserving 2
//
// randomized spawn bots reserving
//
//----------------------------------------------------------------
void SpawnBotReserving2()
{
	int	i,j;

	j = (int)(random() * ListedBots);

	for(i = 0;i < ListedBots; i++,j++)
	{
		if(j >= ListedBots) j = 0;
		if(Bot[j].spflg == BOT_SPAWNNOT)
		{
			Bot[j].spflg = BOT_SPRESERVED;
			SpawnWaitingBots++;

			// lb - check for the lowest team count
			Bot[j].team = G_LowestTeam();

			return;
		}
	}
	SpawnBotReserving();
}

//----------------------------------------------------------------
//Remove Bot
//
// remove bots
//
//	int i	index of bot list
//
//----------------------------------------------------------------
void RemoveBot()
{
	int			i;
	int			botindex;
	edict_t		*e,*ent;
	gclient_t	*client;

	for(i = MAXBOTS - 1;i >= 0;i--)
	{
		if(Bot[i].spflg == BOT_SPAWNED || Bot[i].spflg == BOT_NEXTLEVEL)
		{
			break;
		}
	}

	if(i < 0)
	{
		gi.cprintf (NULL, PRINT_HIGH, "No Bots in server.");
		return;
	}

	botindex = i;

	e = &g_edicts[(int)maxclients->value];
	for ( i = maxclients->value ; i >= 1  ; i--, e--)
	{
		if(!e->inuse) continue;
		client = /*e->client;*/&game.clients[i - 1];
		if(client == NULL) continue;
		// the first couple seconds of server time can involve a lot of
		// freeing and allocating, so relax the replacement policy
		if (!client->pers.connected && (e->svflags & SVF_MONSTER))
		{
			if(client->zc.botindex == botindex)
			{
				if(Bot[botindex].spflg != BOT_NEXTLEVEL) Bot[botindex].spflg = BOT_SPAWNNOT;
				else Bot[botindex].spflg = BOT_SPRESERVED;

				// dont spam at level intermission
				if (!level.intermissiontime)
				{
					gi.bprintf (PRINT_HIGH, "%s disconnected\n", e->client->pers.netname);
				}

				// send effect
				gi.WriteByte (svc_muzzleflash);
				gi.WriteShort (e-g_edicts);
				gi.WriteByte (MZ_LOGOUT);
				gi.multicast (e->s.origin, MULTICAST_PVS);

				e->s.modelindex = 0;
				e->solid = SOLID_NOT;

				if (ctf->value)
				{
					CTFPlayerResetGrapple(e);
					CTFDeadDropFlag(e);
					CTFDeadDropTech(e);
				}

				gi.linkentity (e);

				e->inuse = false;
				G_FreeEdict (e);

				if(targetindex)
				{
					ent = &g_edicts[1];

					if(ent->inuse)
					{
						ent->health = 100;
						ent->movetype = MOVETYPE_WALK;
						ent->takedamage = DAMAGE_AIM;
						ent->target_ent = NULL;
						ent->solid = SOLID_BBOX;
						ent->client->ps.pmove.pm_type = PM_NORMAL;
						ent->client->ps.pmove.pm_flags = PMF_DUCKED;
						VectorCopy(ent->moveinfo.start_origin,ent->s.origin);
						VectorCopy(ent->moveinfo.start_origin,ent->s.old_origin);
					}
					targetindex = 0;
				}
				return;
			}
		}
	}
	gi.error ("Can't remove bot.");
}

//----------------------------------------------------------------
//Level Change Removing
//
// 
//
//----------------------------------------------------------------
void Bot_LevelChange()
{
	int i,j,k;

	j = 0,k = 0;

	for(i = 0;i < MAXBOTS;i++)
	{
		if(Bot[i].spflg)
		{
			if(Bot[i].spflg == BOT_SPAWNED)
			{
				k++;
				Bot[i].spflg = BOT_NEXTLEVEL;
			}
			j++;
		}
	}
	for(i = 0;i < k; i++)
	{
		RemoveBot();
	}

	SpawnWaitingBots = k;//j;
}
